# Linux Utilities #

Set of scripts for simplify Linux administration.

** Memory Info **

Script parse meminfo file and return it with specified units.

Available quantity units of information: [ B KiB MiB GiB TiB PiB EiB ZiB YiB ]

To get maximum integer values use output_unit = "max".

Specify round_factor as quatity of decimals after point.

Tested with Ruby 2.2.4.

*** Install Ruby ***

```
#!bash

sudo yum -y install \
  gcc-c++ \
  openssl-devel \
  patch \
  readline-devel \
  sqlite-devel

printf "gem: --no-document\n" > $HOME/.gemrc

cat >> $HOME/.bash_profile <<'EOF'

export ruby_version=2.2.4
export ruby_home=/u01/software/ruby-$ruby_version
export PATH=$ruby_home/bin:$PATH
EOF
source $HOME/.bash_profile

cd /u01/distr ; wget http://cache.ruby-lang.org/pub/ruby/2.2/ruby-$ruby_version.tar.gz

tar -xzf ruby-$ruby_version.tar.gz ; cd /u01/distr/ruby-$ruby_version

./configure \
--disable-install-doc \
--with-openssl-dir=/usr/include/openssl

make && make install
```

*** Install linux-utils ***

```
#!bash
gem install bundler
git clone https://disa1985@bitbucket.org/disa1985/linux-utils.git
cd linux-utils
bundle install

Usage:
  ./memory_info.rb meminfo_file output_unit round_factor

For example:
  ./memory_info.rb /proc/meminfo "max" 2
  ./memory_info.rb /proc/meminfo "GiB" 2
  ./memory_info.rb /proc/meminfo "MiB" 4

[oracle@localhost linux-utils]$ ./memory_info.rb /proc/meminfo "max" 2
MemTotal            1.81 GiB
MemFree             1.05 GiB
Buffers            22.73 MiB
Cached             50.88 MiB
SwapCached          0.00 B
Active             47.79 MiB
Inactive           46.05 MiB
Active(anon)       20.26 MiB
Inactive(anon)    172.00 KiB
Active(file)       27.53 MiB
Inactive(file)     45.88 MiB
Unevictable         0.00 B
Mlocked             0.00 B
SwapTotal           1.00 GiB
SwapFree            1.00 GiB
Dirty               4.00 KiB
Writeback           0.00 B
AnonPages          20.25 MiB
Mapped              8.25 MiB
Shmem             204.00 KiB
Slab               67.20 MiB
SReclaimable       23.77 MiB
SUnreclaim         43.42 MiB
KernelStack       704.00 KiB
PageTables          2.80 MiB
NFS_Unstable        0.00 B
Bounce              0.00 B
WritebackTmp        0.00 B
CommitLimit         1.61 GiB
Committed_AS       84.63 MiB
VmallocTotal       32.00 TiB
VmallocUsed         4.43 MiB
VmallocChunk       32.00 TiB
HardwareCorrupted   0.00 B
HugePages_Total   600.00 MiB
HugePages_Free    600.00 MiB
HugePages_Rsvd      0.00 B
HugePages_Surp      0.00 B
Hugepagesize        2.00 MiB
DirectMap4k         9.94 MiB
DirectMap2M         1.85 GiB
```