#! /usr/bin/env ruby

require_relative "./lib/convert_information_units.rb"
require_relative "./lib/get_memory_information.rb"

def print_meminfo(memory_info, output_unit, round_factor)
  max_name = 0
  max_value = 0
  memory_info.each do |key, value|
    current_name_length = key.length
    max_name = current_name_length if current_name_length > max_name
    value = convert_information_units(value[:bytes][:unit], value[:bytes][:value], output_unit, round_factor)[1]
    current_value_length = ( "%.#{round_factor}f" % value ).length
    max_value = current_value_length if current_value_length > max_value
  end
  memory_info.each do |key, value|
    unit, value = convert_information_units(value[:bytes][:unit], value[:bytes][:value], output_unit, round_factor)
    print key.ljust(max_name) + " " + ( "%.#{round_factor}f" % value ).rjust(max_value) + " " + unit + "\n"
  end
end

meminfo_file = ARGV[0]
output_unit = ARGV[1]
round_factor = ARGV[2].to_i

memory_info = get_memory_info(meminfo_file, round_factor)

print_meminfo(memory_info, output_unit, round_factor)