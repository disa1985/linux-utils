#!/usr/local/bin/ruby -w

require 'json'

@round_factor = { :KiB => 0, :MiB => 2, :GiB => 4 }

def get_memory_info(meminfo_file)
  # Read /proc/meminfo
  meminfo = File.open(meminfo_file) { |file| file.read }
  # Parse /proc/meminfo to JSON
  memory_info = {}
  meminfo.split("\n").map do |row|
    key = row.split(": ")[0]
    value = row.split(": ")[1].lstrip().split()[0]
    unit = row.split(": ")[1].lstrip().split()[1]
    unless unit.nil?
      memory_info[key] = {
        :KiB =>   value.to_i,
        :MiB => ( value.to_f / 1024 ).round(@round_factor[:MiB]),
        :GiB => ( value.to_f / 1024 / 1024 ).round(@round_factor[:GiB])
      }
    else
      memory_info[key] = { :Pages => value.to_i }
    end
  end
  # Transform hugepages to KiB, MiB, GiB
  memory_info.each do |key, value|
    if key.match(/^HugePages/)
      size_in_kib = ( value[:Pages] * memory_info["Hugepagesize"][:KiB] ).round(@round_factor[:KiB])
      size_in_mib = ( size_in_kib / 1024 ).round(@round_factor[:MiB])
      size_in_gib = ( size_in_mib / 1024 ).round(@round_factor[:GiB])
      memory_info[key][:KiB] = size_in_kib
      memory_info[key][:MiB] = size_in_mib
      memory_info[key][:GiB] = size_in_gib
    end
  end
  memory_info
end

def array_max_element_length(array)
  shift_for_alignment = 1
  ( array.max_by { |element| element.to_s.length } ).to_s.length + shift_for_alignment
end

def print_memory_info(memory_info, unit)
  # https://www.kernel.org/doc/Documentation/filesystems/proc.txt
  # /usr/share/doc/kernel-doc-3.8.13/Documentation/filesystems/proc.txt
  # /usr/share/doc/kernel-doc-3.8.13/Documentation/vm/hugetlbpage.txt

  puts
  puts "** Memory Information **"
  puts

  mem_total = memory_info["MemTotal"][unit]
  mem_buffers = memory_info["Buffers"][unit]
  mem_cached = memory_info["Cached"][unit]
  mem_slab_reclaimable = memory_info["SReclaimable"][unit]
  mem_free = ( memory_info["MemFree"][unit] + mem_buffers + mem_cached + mem_slab_reclaimable ).round( @round_factor[unit] )
  mem_page_tables = memory_info["PageTables"][unit]
  mem_slab_unreclaimable = memory_info["SUnreclaim"][unit]
  mem_used = ( mem_total - mem_free + mem_page_tables + mem_slab_unreclaimable ).round( @round_factor[unit] )
  mem_dirty = memory_info["Dirty"][unit]
  mem_write_back = memory_info["Writeback"][unit]
  mem_active = memory_info["Active"][unit]
  mem_inactive = memory_info["Inactive"][unit]
  mem_slab = memory_info["Slab"][unit]
  mem_maped = memory_info["Mapped"][unit]
  mem_info = [ mem_total, mem_buffers, mem_cached, mem_free, mem_used, mem_dirty, mem_active, mem_inactive, mem_page_tables ]
  mem_rjust = array_max_element_length(mem_info)

  puts "Total       : " + ( "%.#{@round_factor[unit]}f" %    mem_total.to_s ).rjust( mem_rjust ) + " #{unit}"
  puts "Free        : " + ( "%.#{@round_factor[unit]}f" %     mem_free.to_s ).rjust( mem_rjust ) + " #{unit}"
  puts "Used        : " + ( "%.#{@round_factor[unit]}f" %     mem_used.to_s ).rjust( mem_rjust ) + " #{unit}"
  puts
  puts "Buffers     : " + ( "%.#{@round_factor[unit]}f" %  mem_buffers.to_s ).rjust( mem_rjust ) + " #{unit}" + " << Shouldn't get tremendously large (20MB or so)"
  puts "Cached      : " + ( "%.#{@round_factor[unit]}f" %   mem_cached.to_s ).rjust( mem_rjust ) + " #{unit}" + " << Doesn't include SwapCached"
  puts
  puts "Active      : " + ( "%.#{@round_factor[unit]}f" %   mem_active.to_s ).rjust( mem_rjust ) + " #{unit}" + " << Memory that has been used more recently and usually not reclaimed unless absolutely necessary."
  puts "Inactive    : " + ( "%.#{@round_factor[unit]}f" % mem_inactive.to_s ).rjust( mem_rjust ) + " #{unit}" + " << Memory which has been less recently used. It is more eligible to be reclaimed for other purposes"
  puts
  puts "Dirty       : " + ( "%.#{@round_factor[unit]}f" %      mem_dirty.to_s ).rjust( mem_rjust ) + " #{unit}" + " << Memory which is waiting to get written back to the disk"
  puts "Writeback   : " + ( "%.#{@round_factor[unit]}f" % mem_write_back.to_s ).rjust( mem_rjust ) + " #{unit}" + " << Memory which is actively being written back to the disk"
  puts
  puts "Page Tables : " + ( "%.#{@round_factor[unit]}f" % mem_page_tables.to_s ).rjust( mem_rjust ) + " #{unit}" + " << Amount of memory dedicated to the lowest level of page tables. Use hugetables to reduce it."
  puts 
  puts "Slab        : " + ( "%.#{@round_factor[unit]}f" % mem_slab.to_s               ).rjust( mem_rjust ) + " #{unit}" + " << In-kernel data structures cache"
  puts "SReclaimable: " + ( "%.#{@round_factor[unit]}f" % mem_slab_reclaimable.to_s   ).rjust( mem_rjust ) + " #{unit}" + " << Part of Slab, that might be reclaimed, such as caches"
  puts "SUnreclaim  : " + ( "%.#{@round_factor[unit]}f" % mem_slab_unreclaimable.to_s ).rjust( mem_rjust ) + " #{unit}" + " << Part of Slab, that cannot be reclaimed on memory pressure"
  puts
  puts "Mapped      : " + ( "%.#{@round_factor[unit]}f" % mem_maped.to_s              ).rjust( mem_rjust ) + " #{unit}" + " << Files which have been mmaped, such as libraries"

  puts
  puts "** SWAP Information **"
  puts

  swap_total = memory_info["SwapTotal"][unit]
  swap_free = memory_info["SwapFree"][unit]
  swap_cached = memory_info["SwapCached"][unit]
  swap_used = ( memory_info["SwapTotal"][unit] - memory_info["SwapFree"][unit] ).round( @round_factor[unit] )
  swap_info = [ swap_total, swap_free, swap_used ]
  swap_rjust = array_max_element_length(swap_info)

  puts "Total : " + ( "%.#{@round_factor[unit]}f" % swap_total.to_s   ).rjust( swap_rjust ) + " #{unit}"
  puts "Free  : " + ( "%.#{@round_factor[unit]}f" %  swap_free.to_s   ).rjust( swap_rjust ) + " #{unit}" + " << Memory which has been evicted from RAM, and is temporarily on the disk"
  puts "Cached: " + ( "%.#{@round_factor[unit]}f" %  swap_cached.to_s ).rjust( swap_rjust ) + " #{unit}" + " << Memory that once was swapped out, is swapped back in but still also is in the swapfile"
  puts "Used  : " + ( "%.#{@round_factor[unit]}f" %  swap_used.to_s   ).rjust( swap_rjust ) + " #{unit}"

  puts
  puts "** Huge pages Information **"
  puts

  hugepages_size = memory_info["Hugepagesize"][unit]
  hugepages_total_pages = memory_info["HugePages_Total"][:Pages]
  hugepages_free_pages = memory_info["HugePages_Free"][:Pages]
  hugepages_rsvd_pages = memory_info["HugePages_Rsvd"][:Pages]
  hugepages_used_pages = hugepages_total_pages - hugepages_free_pages + hugepages_rsvd_pages
  hugepages_surp_pages = memory_info["HugePages_Surp"][:Pages]
  hugepages_total = memory_info["HugePages_Total"][unit]
  hugepages_free = memory_info["HugePages_Free"][unit]
  hugepages_rsvd = memory_info["HugePages_Rsvd"][unit]
  hugepages_used = ( hugepages_total - hugepages_free + hugepages_rsvd ).round( @round_factor[unit] )
  hugepages_surp = memory_info["HugePages_Surp"][unit]
  hugepages_pages_info = [ hugepages_total_pages, hugepages_free_pages, hugepages_used_pages, hugepages_rsvd_pages, hugepages_surp_pages ]
  hugepages_info = [ hugepages_size, hugepages_total, hugepages_free, hugepages_used, hugepages_rsvd, hugepages_surp ]
  hugepages_pages_rjust = array_max_element_length(hugepages_pages_info)
  hugepages_rjust = array_max_element_length(hugepages_info)

  puts "Page Size: " + ( "%.#{@round_factor[unit]}f" % hugepages_size.to_s ).rjust( hugepages_size.to_s.length ) + " #{unit}"
  puts
  puts "Total   : " + ( hugepages_total_pages.to_s ).rjust( hugepages_pages_rjust ) + " pages" + " = " + ( "%.#{@round_factor[unit]}f" % hugepages_total.to_s ).rjust( hugepages_rjust ) + " #{unit}" + " << Is the size of the pool of huge pages."
  puts "Free    : " + (  hugepages_free_pages.to_s ).rjust( hugepages_pages_rjust ) + " pages" + " = " + ( "%.#{@round_factor[unit]}f" %  hugepages_free.to_s ).rjust( hugepages_rjust ) + " #{unit}" + " << Is the number of huge pages in the pool that are not yet allocated."
  puts "Reserved: " + (  hugepages_rsvd_pages.to_s ).rjust( hugepages_pages_rjust ) + " pages" + " = " + ( "%.#{@round_factor[unit]}f" %  hugepages_rsvd.to_s ).rjust( hugepages_rjust ) + " #{unit}" + " << The number of huge pages for which a commitment to allocate from the pool has been made, but no allocation has yet been made."
  puts "Surplus : " + (  hugepages_surp_pages.to_s ).rjust( hugepages_pages_rjust ) + " pages" + " = " + ( "%.#{@round_factor[unit]}f" %  hugepages_surp.to_s ).rjust( hugepages_rjust ) + " #{unit}" + " << The number of huge pages in the pool above the value in /proc/sys/vm/nr_hugepages."
  puts "Used    : " + (  hugepages_used_pages.to_s ).rjust( hugepages_pages_rjust ) + " pages" + " = " + ( "%.#{@round_factor[unit]}f" %  hugepages_used.to_s ).rjust( hugepages_rjust ) + " #{unit}" + " << The number of huge pages allocated and reserved."

  puts

  #puts JSON.pretty_generate(memory_info)
  #puts JSON.pretty_generate(text_r_adjustment)

end

meminfo_file = ARGV[0]
memory_info = get_memory_info(meminfo_file)
#print_memory_info(memory_info, :KiB)
print_memory_info(memory_info, :GiB)
#print_memory_info(memory_info, :MiB)