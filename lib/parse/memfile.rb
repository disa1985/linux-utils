#! /usr/bin/env ruby

def get_memory_info(meminfo_file, round_factor)
  # Read /proc/meminfo
  meminfo = File.open(meminfo_file) { |file| file.read }
  # Parse /proc/meminfo to JSON
  memory_info = {}
  meminfo.split("\n").map do |row|
    key = row.split(": ")[0]
    value = row.split(": ")[1].lstrip().split()[0]
    unit = row.split(": ")[1].lstrip().split()[1]
    memory_info[key] = {}
    unless unit.nil?
      input_unit = "KiB" if unit == "kB"
      input_value = value
      unit, value = convert_information_units(input_unit, input_value, "B", round_factor)
      memory_info[key][:bytes] = { :unit => unit, :value => value }
    else
      memory_info[key][:pages] = value.to_i
    end
  end
  # Transform hugepages to bytes
  memory_info.each_key do |key|
    if key.match(/^HugePages/)
      hugepage_unit = memory_info["Hugepagesize"][:bytes][:unit]
      hugepage_size = memory_info["Hugepagesize"][:bytes][:value]
      input_value = memory_info[key][:pages] * hugepage_size
      unit, value = convert_information_units(hugepage_unit, input_value, "B", round_factor)
      memory_info[key][:bytes] = { :unit => unit, :value => value }
    end
  end
  memory_info
end