#! /usr/bin/env ruby

def convert_information_units(input_unit, input_value, output_unit, round_factor)
  # Define array of units (index of array = power of 1024)
  units = %w[ B KiB MiB GiB TiB PiB EiB ZiB YiB ]
  max_unit_idx = units.length - 1

  input_unit_idx = units.find_index(input_unit)
  raise ArgumentError, "ERROR: No such input unit in units array!" if input_unit_idx.nil?

  output_value = input_value.to_f

  # If output unit is max - convert to maximum integer unit
  if output_unit == "max"

    output_unit_idx = input_unit_idx
    output_unit = units[output_unit_idx]

    # Do if output unit is not the last unit in array
    # and output value can be converted to larger units
    while output_unit_idx < max_unit_idx && output_value / 1024 >= 1
      output_value = (output_value / 1024).round(round_factor)
      output_unit_idx += 1
    end

    output_unit = units[output_unit_idx]
  else
    output_unit_idx = units.find_index(output_unit)
    raise ArgumentError, "ERROR: No such output unit in units array!" if output_unit_idx.nil?

    diff_unit_idx = output_unit_idx - input_unit_idx
    output_value = output_value / 1024 ** diff_unit_idx
  end

  return output_unit, output_value
end